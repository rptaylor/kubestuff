#!/usr/bin/bash

# For db-fnget test, to set FRONTIER_SERVER automatically.
export SITE_NAME=CA-VICTORIA-WESTGRID-T2

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup emi

echo "Sourcing $BASH_SOURCE complete. If you don't have a valid proxy yet ($X509_USER_PROXY), do 'voms-proxy-init -voms atlas:/atlas/ca --cert <username.p12>'"
echo "Next have a look in kubestuff/jobtests/ ; in particular try './kubestuff/jobtests/WorkerNodeClientTests.sh'"
