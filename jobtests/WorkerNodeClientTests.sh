#!/bin/bash

if [[ "${BASH_SOURCE[0]}" !=  "${0}" ]] ; then
  # Avoid sourcing because it will mess up the environment with ALRB vars
  echo "Please execute this script instead of sourcing it."
  return 1
fi

### TODO - convert this to an Ansible role?
# e.g. see https://github.com/martbhell/ansible-dcache_functionality_test

shopt -s expand_aliases
# make tput work
export TERM=xterm-256color

starttime=`date +%s`

OKSTR="$(tput setaf 2)[ OK ]$(tput sgr0)"
FAILSTR="$(tput setaf 1)[ FAILED ]$(tput sgr0)"

# Whether to actually do any operations
Dryrun=false
# Whether to print verbose output
Verbose=true
# Whether to test the development SE or production SE
Devel=false
vo="atlas"
sumrc=0

while getopts "bnqd" opt
do
  case $opt in
    b)
      vo="belle"
      ;;
    n)
      Dryrun=true
      ;;
    q)
      Verbose=false
      ;;
    d)
      Devel=true
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit -1
      ;;
  esac
done

if [[ "$Verbose" = "true" ]] ; then
  outdev=" >&1"
else
  outdev=" >/dev/null"
fi

test_command () {
  testname="$1"
  testcmd="$2"
  if [[ "$Dryrun" = "true" ]] ; then
    echo "${testcmd}"
  else
    eval echo "$testcmd" "$outdev"
    eval "$testcmd" "$outdev"
    rc=$?
    sumrc=$(($sumrc + ${rc#-}))
    if [[ "$Verbose" = "true" || "$testname" = "SUMMARY" ]] ; then
      if [[ $rc -eq 0 ]] ; then
        echo -e "${testname}\t\t\t ${OKSTR}"
      else
        echo -e "${testname}\t\t\t ${FAILSTR}"
      fi
    fi
    return $rc
  fi
}

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export GLOBUS_FTP_CLIENT_GRIDFTP2=true
#export X509_USER_PROXY=/tmp/x509up_u`id -u`
TESTUSER=rptaylor
#ComputeElement=gorgon03.westgrid.ca
LocalTestFile=/etc/redhat-release
to=90
testnumber="`shuf -i 0-99999 -n 1`"

if [[ "$vo" = "atlas" ]] ; then
  StorageTestLocation="atlasscratchdisk/${TESTUSER}/testdir/test${testnumber}"
  SpaceToken=ATLASSCRATCHDISK

elif [[ "$vo" = "belle" ]] ; then
  StorageTestLocation="belledisk/${TESTUSER}/testdir/test${testnumber}"
  SpaceToken=BELLEDISK

else
  echo "ERROR unknown VO"
  exit 1
fi

if [[ "$Devel" = "true" ]] ; then
  # Test the dev systems
  StorageElement=eos-mgm-0.wlcg-dev.uvic.ca
  Namespace="/eos/wlcg-dev.uvic.ca/data/${vo}"
  # Note: unlike dCache, EOS does not change the root path for webdav, so append namespace
  WebdavDoor="${StorageElement}:8443${Namespace}"
  #FtpDoor=
  #DcapDoor=
  XrootdDoor=$StorageElement
  if [[ "$vo" = "atlas" ]] ; then
    DownloadLargeFile=atlasscratchdisk/rptaylor/testdownloads/DAOD_EXOT27.23317046._000092.pool.root.1
    DownloadSmallFile=atlasscratchdisk/rptaylor/testdownloads/group.phys-higgs.00329780.r10203_p3399_p4165.25542036.PAOD_WH._000224.pool.root
  elif [[ "$vo" = "belle" ]] ; then
    echo "ERROR: VO $vo not supported"
    exit
  fi
else
  # Test the prod systems
  StorageElement=charon01.westgrid.ca
  WebdavDoor="dcache-door.rcs.uvic.ca:2880"
  FtpDoor=basilisk01.westgrid.ca
  Namespace="/pnfs/westgrid.uvic.ca/data/${vo}"
  DcapDoor=basilisk02.westgrid.ca:32125
  XrootdDoor=dcache-door.rcs.uvic.ca
  if [[ "$vo" = "atlas" ]] ; then
    DownloadLargeFile=atlasscratchdisk/rptaylor/testdownloads/DAOD_EXOT27.23317046._000092.pool.root.1
    DownloadSmallFile=atlasscratchdisk/rptaylor/testdownloads/group.phys-higgs.00329780.r10203_p3399_p4165.25542036.PAOD_WH._000224.pool.root
  elif [[ "$vo" = "belle" ]] ; then
    DownloadLargeFile=belledisk/DATA/belle/MC/release-04-00-03/DB00000757/MC13a/prod00009702/s00/e1003/4S/r00000/1293710005/mdst/sub00/mdst_000031_prod00009702_task10020000030.root
    DownloadSmallFile=belledisk/DATA/belle/MC/build-2018-06-12/DB00000396/SKIMDC1x1/prod00005058/e0000/4S/r00000/dc/BtoDh_Kspi0/udst/sub00/udst_000406_prod00005058_task00000406.root
  fi
fi

SmallFileName=`basename $DownloadSmallFile`

#Interesting idea: prometheus.desy.de is an open dCache server, always running latest dev release. Could use for validating client functionality. 

# For reference, here is a good ALRB/Rucio transfer test. 
# TODO automate this
## set up ALRB
# export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
# source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
# diagnostics  
# export RUCIO_ACCOUNT=rptaylor
# rseCheck CA-VICTORIA-WESTGRID-T2_SCRATCHDISK

# Other ALRB tests, less useful. This is more of a test of ALRB itself...
# It kind of reproduces my test upload/download, but via rucio, which is likely what an end user would do.
# Also provides a nice overall summary of successes/failures, like this script.
# Could be useful to test aspects of ALRB/Rucio, but usually I want to do direct low-level tests of the storage.
# < set up ALRB>
# export ALRB_testRucioRseList="CA-VICTORIA-WESTGRID-T2_SCRATCHDISK"
# export RUCIO_ACCOUNT=rptaylor
# diagnostics
# toolTest -m user root  -v  # does some local tests reading/writing files in /tmp with ROOT (less useful)
# toolTest -m user root xrootd -v  # also test xrootd transfers
##  maybe others 

# Test proxy validity
test_command "Validating VOMS proxy" "voms-proxy-info -exists" || exit
test_command "Checking VO of proxy" "test \"`voms-proxy-info --vo`\" = \"${vo}\"" || exit

# gfal (SRM) commands
#test_command "gfal copy in file" \
#  "gfal-copy --verbose -t $to -D \"SRM PLUGIN:TURL_PROTOCOLS=gsiftp\" -S $SpaceToken file://$LocalTestFile srm://$StorageElement:8443/srm/managerv2?SFN=$Namespace/$StorageTestLocation/gfal-testfile"
#test_command "gfal ls copied file" \
#  "gfal-ls --verbose -t $to -l srm://$StorageElement:8443/srm/managerv2?SFN=$Namespace/$StorageTestLocation/gfal-testfile"
#test_command "gfal copy out file" \
#  "gfal-copy --verbose -t $to -D \"SRM PLUGIN:TURL_PROTOCOLS=gsiftp\" -f srm://$StorageElement:8443/srm/managerv2?SFN=$Namespace/$StorageTestLocation/gfal-testfile file:///dev/null"
#test_command "gfal delete file" \
#  "gfal-rm --verbose -t $to srm://$StorageElement:8443/srm/managerv2?SFN=$Namespace/$StorageTestLocation/gfal-testfile"

# gfal (WebDAV) commands
test_command "gfal copy in file" \
  "gfal-copy --verbose --parent -t $to  -S $SpaceToken file://$LocalTestFile davs://$WebdavDoor/$StorageTestLocation/gfal-testfile"
test_command "gfal ls copied file" \
  "gfal-ls --verbose -t $to -l davs://$WebdavDoor/$StorageTestLocation/gfal-testfile"
test_command "gfal copy out file" \
  "gfal-copy --verbose -t $to -f davs://$WebdavDoor/$StorageTestLocation/gfal-testfile file:///dev/null"
test_command "gfal delete file" \
  "gfal-rm --verbose -t $to davs://$WebdavDoor/$StorageTestLocation/gfal-testfile"
test_command "gfal delete directory recursively" \
  "gfal-rm --verbose -t $to -r davs://$WebdavDoor/$StorageTestLocation/"

# gridftp
#if [[ "$vo" = "atlas" ]] ; then
#  test_command "gridftp from CE" "globus-url-copy -rst-timeout 5 -rst-retries 1 gsiftp://$ComputeElement:2811/etc/redhat-release -"
#fi

# globus is deprecated, don't use globus-url-copy anymore. Also affected by proxy secret ownership issue: https://github.com/kubernetes/kubernetes/issues/81089
#test_command "gridftp from door node" "globus-url-copy -rst-retries 1 gsiftp://$FtpDoor:2811/$Namespace/$DownloadSmallFile file:///dev/null"

# dccp
#test_command "dccp copy" "dccp dcap://$DcapDoor/$Namespace/$DownloadLargeFile /dev/null"

# dcap
#export LD_PRELOAD="${GLITE_LOCATION}/lib64/libpdcap.so"
#test_command "dcap copy" \
#  "/bin/cp dcap://$DcapDoor/$Namespace/$DownloadSmallFile /dev/null" 
#unset LD_PRELOAD

# srm
#test_command "srmmkdir" \
#  "srmmkdir srm://$StorageElement:8443/srm/managerv2?SFN=$Namespace/$StorageTestLocation/srmtestdir"
#test_command "srmcp in" \
#  "srmcp -2 -retry_num=2 file:///$LocalTestFile srm://$StorageElement:8443/srm/managerv2?SFN=$Namespace/$StorageTestLocation/srmtestdir/srmtestfile"
#test_command "srmls" \
#  "srmls srm://$StorageElement:8443/srm/managerv2?SFN=$Namespace/$StorageTestLocation/srmtestdir/srmtestfile"
#test_command "srmrm" \
#  "srmrm srm://$StorageElement:8443/srm/managerv2?SFN=$Namespace/$StorageTestLocation/srmtestdir/srmtestfile"
#test_command "srmrmdir" \
#  "srmrmdir srm://$StorageElement:8443/srm/managerv2?SFN=$Namespace/$StorageTestLocation/srmtestdir"

#test_command "clean up test dir" \
#  "srmrmdir srm://$StorageElement:8443/srm/managerv2?SFN=$Namespace/$StorageTestLocation"

rm -f /$TMPDIR/$SmallFileName
# curl
test_command "curl download from WebDAV door" \
  "curl --show-error --connect-timeout 300 --max-time 3600 --cacert $X509_USER_PROXY --capath $X509_CERT_DIR --cert $X509_USER_PROXY --key $X509_USER_PROXY -L https://$WebdavDoor/$DownloadSmallFile -o /$TMPDIR/$SmallFileName -w \"%{url_effective}\n\""
test_command "curl verify downloaded file" "file -b /$TMPDIR/$SmallFileName | grep ROOT"

#test_command "curl upload to WebDAV door" "curl -E $X509_USER_PROXY --capath $X509_CERT_DIR -L https://$StorageElement:2880/$StorageTestLocation/$testfilename -T $LocalTestFile"

test_command "xrdcp" "xrdcp -f root://$XrootdDoor/$Namespace/$DownloadLargeFile /dev/null"

### TO TEST DIRECT XROOTD ACCESS
###  /cvmfs/atlas.cern.ch/repo/sw/local/xrootdsetup.sh

# ROOT
cat << EOF > /$TMPDIR/rootdataaccess.C
void rootdataaccess() {
//  TFile *f1 = TFile::Open("dcap://${DcapDoor}/${Namespace}/${DownloadSmallFile}");
//  f1->ls();
//  Long64_t size1=f1->GetSize(); 
//  printf("Size: %lli bytes.\n", size1);
//  f1->Close();

  TFile *f2 = TFile::Open("root://${XrootdDoor}/${Namespace}/${DownloadSmallFile}");
  f2->ls();
  Long64_t size2=f2->GetSize(); 
  printf("Size: %lli bytes.\n", size2);
  f2->Close();
}
EOF
if [[ "$Verbose" = "true"  ]] ; then
  cat /$TMPDIR/rootdataaccess.C
fi

# lsetup in separate shell to avoid env pollution
test_command "ROOT access" \
  "bash -c 'source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet; lsetup \"root recommended\" --quiet; root -b -l -q  /$TMPDIR/rootdataaccess.C'"
rm -f /$TMPDIR/rootdataaccess.C

test_command "SRR accounting" \
  "bash -c 'source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet; lsetup \"adctools\" --quiet; checkSRR.sh site=CA-VICTORIA-WESTGRID-T2'"

# Frontier Squid
# db-fnget always returns 0 so just run it this way
#source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet && source ${ATLAS_LOCAL_ROOT_BASE}/packageSetups/atlasLocalDiagnostics.sh --quiet && source ${VO_ATLAS_SW_DIR}/local/setup.sh && ${ATLAS_LOCAL_ROOT_BASE}/utilities/fngetTest.sh


endtime=`date +%s`
test_command "SUMMARY" "test $sumrc -eq 0"
echo "Completed in $((endtime-starttime)) seconds"


